public Access removeProjectPermission()
{
    if (permissions.isAdministrator(user)) {
      return GRANTED;
    }
    return DENIED;
}
