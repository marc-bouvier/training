# Work in progress

## Crafting with hexagonal architecture

- [Java](https://gitlab.com/crafting-software/training/builds/artifacts/master/file/crafting_hexagonal/slides-java.pdf?job=training) [[notes](https://gitlab.com/crafting-software/training/builds/artifacts/master/file/crafting_hexagonal/slides-java-with-notes.pdf?job=training)]


## Introduction to Domain Driven Design

- [Java](https://gitlab.com/crafting-software/training/builds/artifacts/master/file/crafting_ddd_introduction/slides-java.pdf?job=training) [[notes](https://gitlab.com/crafting-software/training/builds/artifacts/master/file/crafting_ddd_introduction/slides-java-with-notes.pdf?job=training)]



# Ideas

- Tactical DDD
- Style de programmation: oop / fp / ...
- Living documentation
- Tests fonctionnels / e2e / gherking
- Clean code
- model de déploiement: micro service vs monolith
- Object-Oriented fundamentals (optional preamble session).
- Pair programming.
- Refactoring techniques.
- Code Smells.
- Legacy code.
- Four elements of simple design.
- Outside in TDD.
- Design Patterns (optional).
- Domain Driven Design introduction (optional).
